from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from ViewModels.UnosPacijenta import UnosPacijenta
from ViewModels.PopisPacijenata import PopisPacijenata
from ViewModels.UnosLijecnika import UnosLijecnika
from ViewModels.PopisLijecnika import PopisLijecnika
from ViewModels.PopisGrafikona import PopisGrafikona

class GlavniProzor(QMainWindow):
    def __init__(self,sesija):
        super().__init__()
        self.sesija = sesija

        uic.loadUi("./Views/glavniProzor.ui",self)

        self.B_Izlaz.clicked.connect(self.close)
        self.B_UnosPacijenta.clicked.connect(self.unosPacijenta)
        self.B_PopisPacijenata.clicked.connect(self.popisPacijenata)
        self.B_UnosLijecnika.clicked.connect(self.unosLijecnika)
        self.B_PopisLijecnika.clicked.connect(self.popisLijecnika)
        self.grafikoniButton.clicked.connect(self.popisGrafikona)

     
    def unosPacijenta(self):
        self.prozorUnosPacijenata = UnosPacijenta(self.sesija, self.show)
        self.prozorUnosPacijenata.show()
        self.close()

    def popisPacijenata(self):
        self.prozorPopisPacijenata = PopisPacijenata(self.sesija, self.show)
        self.prozorPopisPacijenata.show()
        self.close()
        

    def unosLijecnika(self):
        self.prozorUnosLijecnika = UnosLijecnika(self.sesija, self.show)
        self.prozorUnosLijecnika.show()
        self.close()

    def popisLijecnika(self):
        self.prozorPopisLijecnika = PopisLijecnika(self.sesija, self.show)
        self.prozorPopisLijecnika.show()
        self.close()

    def popisGrafikona(self):
        self.prozorPopisGrafikona = PopisGrafikona(self.sesija, self.show)
        self.prozorPopisGrafikona.show()
        self.close()

