from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from ViewModels.GlavniProzor import GlavniProzor

class LoginProzor(QMainWindow):
    def __init__(self,sesija):
        super().__init__()
        self.sesija = sesija

        uic.loadUi("./Views/loginProzor.ui",self)

        self.prijavaButton.clicked.connect(self.Prijava)
    

    def Prijava(self):
        unosKorisnika = self.lozinkaTekst.text().strip()

        if unosKorisnika != "administrator":
           porukaProzor = QMessageBox()
           porukaProzor.setIcon(QMessageBox.Warning) 
           porukaProzor.setText("Pogrešna lozinka!")
           porukaProzor.setStandardButtons(QMessageBox.Ok)
           porukaProzor.exec()
           self.lozinkaTekst.setText("")
        else:
            self.GlavniProzor = GlavniProzor(self.sesija)
            self.GlavniProzor.show()
            self.close()
