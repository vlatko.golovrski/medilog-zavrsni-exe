from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
import os


class PrikazGrafikona(QWidget):
    def __init__(self, povratak):
        super().__init__()
        uic.loadUi("./Views/prikazGrafikona.ui",self)


        self.povratakButton.clicked.connect(self.close)
        self.povratakButton.clicked.connect(povratak)
        self.povratakButton.clicked.connect(lambda: os.remove("./Views/grafikon.jpg"))
