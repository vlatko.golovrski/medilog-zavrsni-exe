from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.Lijecnik import Lijecnik
from Models.Pacijent import Pacijent
from ViewModels.PrikazGrafikona import PrikazGrafikona
from sqlalchemy import func
from sqlalchemy.orm import joinedload, relationship
import pandas as pd
import matplotlib.pyplot as plt
from collections import Counter






class PopisGrafikona(QWidget):
    def __init__(self,sesija, povratak):
        super().__init__()
        uic.loadUi("./Views/popisGrafikona.ui",self)
        self.sesija = sesija

        self.povratakButton.clicked.connect(self.close)
        self.povratakButton.clicked.connect(povratak)

        self.muskiZenskiPacijentiButton.clicked.connect(self.muskiZenskiPacijenti)
        self.muskiZenskiLijecniciButton.clicked.connect(self.muskiZenskiLijecnici)
        self.brojPacijenataPoLijecnikuButton.clicked.connect(self.brojPacijenataPoLijecniku)


    
    def muskiZenskiPacijenti(self):
        podaci = self.sesija.query( Pacijent.SPOL, func.count(Pacijent.ID)).group_by(Pacijent.SPOL).all()
        spol = [item[0] for item in podaci]
        brojac = [item[1] for item in podaci]

        colors = ['#66b3ff','#ff9999']
        plt.bar(spol, brojac, color = colors)

        plt.title('Odnos muških i ženskih pacijenata')
        plt.xlabel('SPOL')
        plt.ylabel('BROJ')
   
        for i, v in enumerate(brojac):
            plt.text(i, v + -0.5, str(v), color='black', fontweight='bold', ha='center')

        plt.xticks(spol)
        plt.ylim(0, max(brojac) + 1)
        plt.yticks(range(max(brojac) + 1))   
        plt.savefig("./Views/grafikon.jpg", dpi=300)

        self.prozorGrafikon = PrikazGrafikona(self.show)
        self.prozorGrafikon.show()
        self.close()
        plt.clf()



    def muskiZenskiLijecnici(self):
        
        podaci = self.sesija.query( Lijecnik.SPOL, func.count(Lijecnik.ID)).group_by(Lijecnik.SPOL).offset(1).all()
        spol = [item[0] for item in podaci]
        brojac = [item[1] for item in podaci]

        colors = ['#66b3ff','#ff9999']
        plt.bar(spol, brojac, color = colors)

        plt.title('Odnos muških i ženskih liječnika')
        plt.xlabel('SPOL')
        plt.ylabel('BROJ')
   
        for i, v in enumerate(brojac):
            plt.text(i, v + -0.5, str(v), color='black', fontweight='bold', ha='center')

        plt.xticks(spol)
        plt.ylim(0, max(brojac) + 1)
        plt.yticks(range(max(brojac) + 1))   
        plt.savefig("./Views/grafikon.jpg", dpi=300)

        self.prozorGrafikon = PrikazGrafikona(self.show)
        self.prozorGrafikon.show()
        self.close()
        plt.clf()
        
    def brojPacijenataPoLijecniku(self):
        spojeniPodaci = self.sesija.query(Pacijent, Lijecnik).\
                    filter(Pacijent.ODABIRDOKTORA == Lijecnik.ID).all()

        brojPacijenataPoLijecniku = {}

        for pacijent, lijecnik in spojeniPodaci:
            imeLijecnika = f"{lijecnik.IME} {lijecnik.PREZIME}"
            if imeLijecnika in brojPacijenataPoLijecniku:
                brojPacijenataPoLijecniku[imeLijecnika] += 1
            else:
                brojPacijenataPoLijecniku[imeLijecnika] = 1

        x = list(brojPacijenataPoLijecniku.keys())
        y = list(brojPacijenataPoLijecniku.values())

        plt.bar(x, y)

        plt.title("Broj pacijenata po liječniku")
        plt.xlabel("Liječnik")
        plt.ylabel("Broj pacijenata")
        plt.xticks(rotation=15, ha="right")

        for i, v in enumerate(y):
            plt.text(i, v + -0.5, str(v), color='black', fontweight='bold', ha='center')

        plt.xticks(x)
        plt.ylim(0, max(y) + 1)
        plt.yticks(range(max(y) + 1))   

        plt.savefig("./Views/grafikon.jpg", dpi=300)
        
        
        self.prozorGrafikon = PrikazGrafikona(self.show)
        self.prozorGrafikon.show()
        self.close()
        plt.clf()
      
